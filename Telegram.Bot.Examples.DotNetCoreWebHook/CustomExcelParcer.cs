using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Telegram.Bot.Examples.DotNetCoreWebHook
{

    class CustomExcelParcer
    {
         public int Parse(string path)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            List<Groupe> groupes = new List<Groupe>();

            using (var package = new ExcelPackage(new FileInfo("d://data.xlsx")))
            {
                foreach (var item23 in package.Workbook.Worksheets)
                {
                    var firstSheet = item23;
                    List<ExcelRangeBase> list = new List<ExcelRangeBase>();
                    var temp = firstSheet.Cells["F11"];
                    List<string> data = new List<string>();

                    Dictionary<Position, Position> positionNamberWiik = new Dictionary<Position, Position>();
                    for (int i = 1; i < 635; i++)
                    {
                        for (int j = 1; j < 56; j++)
                        {
                            if (firstSheet.Cells[i, j].Text.Contains("Розклад занять на"))
                            {
                                list.Add(firstSheet.Cells[i + 1, 4]);
                                int startPositionData = 10;

                                data.Add(firstSheet.Cells[i + 1, startPositionData].Text);
                                data.Add(firstSheet.Cells[i + 1, startPositionData += 8].Text);
                                data.Add(firstSheet.Cells[i + 1, startPositionData += 8].Text);
                                data.Add(firstSheet.Cells[i + 1, startPositionData += 8].Text);
                                data.Add(firstSheet.Cells[i + 1, startPositionData += 8].Text);
                                data.Add(firstSheet.Cells[i + 1, startPositionData += 8].Text);

                                var copuI = i + 1;

                                bool flug = true;
                                while (flug)
                                {
                                    if (firstSheet.Cells[copuI, 4].Style.Border.Left.Style == (ExcelBorderStyle.None))
                                    {
                                        flug = false;
                                        positionNamberWiik.Add(new Position(i + 1, 4), new Position(copuI - 1, 4));
                                    }
                                    copuI++;
                                }
                            }
                        }
                    }

                    List<Position> positionsnumbeGroup = new List<Position>();

                    foreach (var item in positionNamberWiik)
                    {
                        int countgroupe = (item.Value.x - item.Key.x - 1) / 4;
                        Position firstPosition = new Position(item.Key.x + 3, item.Key.y);
                        positionsnumbeGroup.Add(firstPosition);
                        for (int i = 0; i < countgroupe - 1; i++)
                        {
                            positionsnumbeGroup.Add(new Position(positionsnumbeGroup[positionsnumbeGroup.Count - 1].x + 4, positionsnumbeGroup[positionsnumbeGroup.Count - 1].y));
                        }
                    }

                    foreach (var item in positionsnumbeGroup)
                    {
                        var tgroup = new Groupe();
                        int startx = item.x - 1;
                        int statty = item.y + 1;
                        int count = 0;
                        bool flug2 = false;

                        tgroup.NameGroupe = firstSheet.Cells[item.x, item.y].Text;

                        int flugLeson = 0;

                        int carendDataIndex = 0;

                        for (int i = 0; i < 24; i++)
                        {
                            int yleson = statty + (i * 2);

                            var leson = new Leson();

                            flugLeson++;

                            leson.Namber = flugLeson;

                            if (flugLeson % 4 == 0)
                            {
                                flugLeson = 0;

                                leson.Data = data[carendDataIndex];
                                carendDataIndex++;
                            }
                            else
                            {
                                leson.Data = data[carendDataIndex];
                            }

                            leson.Group = firstSheet.Cells[item.x, item.y].Text;
                            leson.Nmae = firstSheet.Cells[startx, yleson].Text;
                            leson.Team = firstSheet.Cells[startx, yleson + 1].Text;
                            leson.TeacherNmaes += firstSheet.Cells[startx + 1, yleson].Text;
                            leson.TeacherNmaes += firstSheet.Cells[startx + 2, yleson].Text;
                            leson.Classroom += firstSheet.Cells[startx + 3, yleson].Text;
                            leson.Classroom += firstSheet.Cells[startx + 3, yleson + 1].Text;

                            tgroup.Lesons.Add(leson);
                        }
                        groupes.Add(tgroup);
                    }
                }
            }
            Console.WriteLine("s");
            return groupes.Count;
        }

        public class Position
        {
            public List<String> Data { get; set; }
            public int x { get; set; }
            public int y { get; set; }
            public Position(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        public class Leson
        {
            public string Group { get; set; }
            public string Data { get; set; }
            public string Nmae { get; set; }
            public string TeacherNmaes { get; set; }
            public string Classroom { get; set; }
            public string Team { get; set; }
            public int Namber { get; set; }
        }

        public class Groupe
        {
            public string NameGroupe { get; set; }
            public List<Leson> Lesons { get; set; } = new List<Leson>();
        }
    }
}
