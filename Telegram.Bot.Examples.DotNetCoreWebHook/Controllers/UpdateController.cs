using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Examples.DotNetCoreWebHook.Services;
using Telegram.Bot.Types;

namespace Telegram.Bot.Examples.DotNetCoreWebHook.Controllers
{
    [Route("api/[controller]")]
    //api/Update
    public class UpdateController : Controller
    {
        private readonly IUpdateService _updateService;

        public UpdateController(IUpdateService updateService)
        {
            _updateService = updateService;
        }

        // POST api/update
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Update update)
        {
            CustomExcelParcer customExcelParcer = new CustomExcelParcer();
            update.Message.Text = customExcelParcer.Parse("noth").ToString();
            await _updateService.EchoAsync(update);
            return Ok();
        }
    }
}
